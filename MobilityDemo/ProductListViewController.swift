
import UIKit

class ProductListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
 
   private var arrayProduct: [Product] = []
   private var cellId = "Cell"

    override func viewDidLoad() {
        super.viewDidLoad()
        title = appTitle
        addCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.global().async {
            self.getData()
        }
    }
    
    lazy var productCollection: UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        layout.minimumInteritemSpacing = 4
        layout.itemSize = CGSize(width:(view.bounds.size.width - 26)/2,height: (view.bounds.size.height)/3)
        
       let collectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(ProductCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.backgroundColor = UIColor.groupTableViewBackground
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
}

//MARK:: Delegate and DataSource metod call

extension ProductListViewController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayProduct.count
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = productCollection.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ProductCell
        cell.nameLabel.text = arrayProduct[indexPath.row].author
        let strOfUrl = "https://picsum.photos/300/300?image=\(arrayProduct[indexPath.row].id ?? 9999)"
        guard  let url = URL(string: strOfUrl) else {
            return cell
        }
        cell.productImg.downloaded(from: url, contentMode: .scaleToFill)
        return cell
     }
    
    
}
//MARK:: Collection view create
extension ProductListViewController {
    private func addCollectionView() {
        self.view.addSubview(productCollection)
        productCollection.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        productCollection.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        productCollection.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        productCollection.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}

//MARK:: Service Call
extension ProductListViewController {
   private func getData() {
        ProductListGet { (products) in
            DispatchQueue.main.async {
                self.arrayProduct = products
                self.productCollection.reloadData()
            }
        }
    }
}
