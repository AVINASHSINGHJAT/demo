
import Foundation

struct Product {
    var format: String?
    var id: Int?
    var author: String?
    
    init(peremater: [String:Any]) {
        self.format = peremater["format"] as? String
        self.id = peremater["id"] as? Int ?? 0
        self.author = peremater["author"] as? String ?? ""
    }
}
