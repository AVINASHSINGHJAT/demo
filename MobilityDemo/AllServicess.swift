
import Foundation

let baseUrl = "https://picsum.photos"

let productUrl = "\(baseUrl)/list"
let imgUrl = "\(baseUrl)/300/300?image="




func ProductListGet(completionHandler: @escaping ([Product]) -> Void) {
  let url = URL(string: productUrl)!

    var listProduct : [Product] = []
  let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
    if let error = error {
      print("Error with fetching product: \(error)")
      return
    }
    
    do {
        let obj = try JSONSerialization.jsonObject(with: data!, options: [])
        for items in (obj as? Array<Any>)!{
            let productModel = Product.init(peremater: items as! [String : Any])
            listProduct.append(productModel)
        }
    }catch(let err) {
        print(err.localizedDescription)
    }
       completionHandler(listProduct)
  })
  task.resume()
}
