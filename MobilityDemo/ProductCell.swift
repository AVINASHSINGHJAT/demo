
import UIKit

class ProductCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let productImg: UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    func addViews() {
        backgroundColor = UIColor.white
        addSubview(productImg)
        addSubview(nameLabel)
        
        productImg.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        productImg.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        productImg.topAnchor.constraint(equalTo: topAnchor).isActive = true
        productImg.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -30).isActive = true
        
        nameLabel.topAnchor.constraint(equalTo: productImg.bottomAnchor).isActive = true
        nameLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 5).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -5).isActive = true
        
    }
}
